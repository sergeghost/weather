/* Core */
import axios from "axios";

const WEATHER_API_URL = process.env.REACT_APP_WEATHER_API_URL;

export const api = {
    async getWeather() {
        return await axios(`${WEATHER_API_URL}`).then(res => res.data);
    },
};
