import React from "react";
const classnames = require("classnames");
import WeatherModel from "../models/WeatherModel";

const daysList = [
    'Воскресенье',
    'Понедельник',
    'Вторник',
    'Среда',
    'Четверг',
    'Пятница',
    'Суббота'
];

interface ForecastDayProps {
    weather: WeatherModel
}

export const ForecastDay: React.FC<ForecastDayProps> = (props) => {
    const { weather } = props;

    return (
        <div className={classnames('day')}>
            <p>{daysList[new Date(weather.date).getDay()]}</p>
            <span>{weather.values.temperature.toFixed()}</span>
        </div>
    );

};
