import {useQuery} from "react-query";
import {api} from "../api";

export const useFetchForecast = () => useQuery('weather', api.getWeather);

